# For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

Rails.application.routes.draw do
  root to: "main#index"

  get "sign-up", to: "registrations#new"
  post "sign-up", to: "registrations#create"
  
  get "sign-in", to: "sessions#new"
  post "sign-in", to: "sessions#create"

  get "password", to: "passwords#edit", as: :edit_password
  patch "password", to: "passwords#update"

  get "password/reset", to: "passwords_resets#new"
  post "password/reset", to: "passwords_resets#create"
  get "password/reset/edit", to: "passwords_resets#edit"
  patch "password/reset/edit", to: "passwords_resets#update"
  
  delete "logout", to: "sessions#destroy"

  get "auth/twitter/callback", to: "omniauth_callbacks#twitter"

  resources :twitter_accounts
  resources :tweets

  get "about", to: "pages#about"

  require 'sidekiq/web'

  ScheduledTweets::Application.routes.draw do
    mount Sidekiq::Web => "/sidekiq"
  end
end
